# Two Sum LeetCode
# Beats 91.70% for runtime
# beats 75.13% for RAM
def twoSum(nums, target):
    num_dict = {}
    for i in range(len(nums)):
        num = nums[i]
        complement = target - num
        if complement in num_dict:
            return [num_dict[complement], i]
        num_dict[num] = i
    return []


# Palindrome Number
def isPalindrome(x):
    num = x
    rev = 0
    while x > 0:
        a = x % 10
        rev = rev * 10 + a
        x = x // 10
    if rev == num:
        return True
    else:
        return False


# Roman to Integer
def romanToInt(s):
    roman_dict = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}

    result = 0
    prev_value = 0
    n = len(s)

    for i in range(n - 1, -1, -1):
        current_char = s[i]

        current_value = roman_dict[current_char]

        if current_value < prev_value:
            result -= current_value
        else:
            result += current_value

        prev_value = current_value

    return result


# Longest Common Prefix
def longestCommonPrefix(strs):
    if not strs:
        return ""
    strs.sort()
    first_str = strs[0]
    second_str = strs[1]
    common_prefix = []
    for i in range(len(first_str)):
        if i < len(second_str) and first_str[i] == second_str[i]:
            common_prefix.append(first_str[i])
        else:
            break
    return "".join(common_prefix)


# Check if two string arrays are equivalent
# Beats 99.32% for runtime
# beats 83.79% for RAM
def arrayStringsAreEqual(word1, word2):
    sep = ""
    joinedword1 = sep.join(word1)
    joinedword2 = sep.join(word2)
    if joinedword1 == joinedword2:
        return True
    else:
        return False


arrayStringsAreEqual(
    ["ab", "c"],
    ["ab", "c"],
)
