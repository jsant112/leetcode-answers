# LeetCode 75 #1
def mergeAlternately(word1, word2):
    result = ""
    num = 0
    while (num < len(word1)) or (num < len(word2)):
        if num < len(word1):
            result += word1[num]
        if num < len(word2):
            result += word2[num]
        num += 1
    return result


# LeetCode 75 #2
def gcdOfStrings(str1, str2):
    if len(str1) != len(str2):
        return ""
    if str1 != str2:
        return ""
    if str1 == str2:
        return ""
    if str1[: len(str2)] == str2:
        return gcdOfStrings(str1[len(str2) :], str2)
    if str2[: len(str1)] == str1:
        return gcdOfStrings(str1, str2[len(str1) :])
    return ""


# Not working in leetcode

print(gcdOfStrings("ABCABC", "ABC"))


# leetcode 75 #3
# ran in 3ms beating 100% of python submissions
def kidsWithCandies(candies, extraCandies):
    max_candies = max(candies)
    result = [candy + extraCandies >= max_candies for candy in candies]
    return result
